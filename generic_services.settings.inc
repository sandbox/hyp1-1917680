<?php


function generic_services_admin() {
	$page = array();
	$page[] = drupal_get_form('generic_services_admin_add');
	$page[] = drupal_get_form('generic_services_admin_methods_table');
	return $page;
}

function generic_services_admin_settings() {
	$page = array();
	$page[] = drupal_get_form('generic_services_admin_add');
	$page[] = drupal_get_form('generic_services_admin_methods_table');
	return $page;
}


function generic_services_admin_methods_table_submit($form,&$form_state){
	$methods=$form['methods']['table']['#options'];
	for( $i = 0; $i < sizeof($methods); $i++ ){
		if($form['methods']['table'][$i]['#value'] != 0)
			//$methods[$i][0] = service_name
			_generic_services_delete_method($methods[$i][0]);
	}
}



/**
 *
 * @return Drupal Table of service methods
 */
function generic_services_admin_methods_table($form,&$form_state){
	
	//prettify, if we can
	if(module_exists("prettify"))prettify_add_library();

	$options=_generic_services_get_types();

	$methods=db_query("SELECT service_name,service_type,method_call,parameters,description FROM {gs_methods}");

	$header = array(t('Name'), t('Type'), t('Method call'), t('Parameter'), t('Description'),t('Operations'));
	$rows = array();
	$cnt=0;
	foreach($methods as $method){
		$rows[]=array(
				$method->service_name,
				$options[$method->service_type],
				'<pre><code>'.$method->method_call.'</pre></code>',
				$method->parameters,
				$method->description,
				l('Edit','admin/config/services/generic/'.$method->service_name.'/edit').'&nbsp;'.
				l('Execute','services/generic/'.$method->service_name).				
				l('Debug','admin/config/services/generic/'.$method->service_name.'/debug')
		);
		$cnt++;
	}
	$form=array();

	$form['methods']['table']=array('#type'=>'tableselect',
			'#header'=>$header,
			'#options'=>$rows);

	$form[] = array(
			'#type'=>'submit',
			'#description' => t('Add') . t('to your stock portfolio.'),
			'#value' => t('Ausgewählte Entfernen') . ' ' ,
			'#submit' => array('generic_services_admin_methods_table_submit')
	);

	return $form;
}


function generic_services_admin_add_validate($form,&$form_state){

	$s_type=$form['gs']['service']['servicetype']['#value'];
	$s_name=$form['gs']['service']['servicename']['#value'];
	$s_command=$form['gs']['service']['command']['#value'];
	$s_params=$form['gs']['service']['parameters']['#value'];
	$s_desc=$form['gs']['service']['description']['#value'];

	if(_generic_services_check_method_exists($s_name)){
		form_set_error('servicename',t('Dieser Service Name existiert bereites'));
	};

}



function generic_services_admin_method_edit_submit($form, &$form_state){
	$method=new StdClass();

	$method->type=$form_state['input']['servicetype'];
	// service name is disabled so it is in values!
	$method->name=$form_state['values']['servicename'];
	//add slashes for call
	$method->call= addslashes($form_state['input']['command']);
	$method->parameters=$form_state['input']['parameters'];
	$method->description=$form_state['input']['description'];

	_generic_services_update_method($method);
	drupal_goto('admin/config/services/generic');

}

function generic_services_admin_method_edit(){
	unset($_SESSION['generic_services_debug_args']);

	$method=_generic_services_load_method_call(arg(4));

	$form['gs']["service2"] = array(
			'#type' => 'fieldset',
			'#title' => t('Change Generic Service'),
			'#description' => t("change the method calls of your Generic Service "),
	);

	//ACHTUNG GENERIC_SERICES_FUNC_TYPES !!!
	$options=array(0=>'SQL Statement',1=>'Drupal Funktion',2=>'PHP Code',3=>'Datei Operation');

	$form['gs']["service2"]["servicetype"] = array(
			'#type' => 'select',
			'#title' => t('Type'),
			'#options' => $options,
			'#description' => t("choose execution type"),
			'#required' => TRUE,
			'#default_value' => array((int)$method->type),
	);


	$form['gs']["service2"]['servicename'] = array(
			'#type' => 'textfield',
			'#title' => t('Servicename'),
			'#description' => t("please give a machine readable name"),
			'#required' => TRUE,
			'#disabled' => TRUE,
			'#value' => $method->name,
	);


	$form['gs']["service2"]["command"] = array(
			'#type' => 'textarea',
			'#title' => t('Functioncalls'),
			'#description' => t("use anonymous parameters as: #PARAM1 #PARAM2 #PARAM3, ... they will be replaced by the incoming parameters"),
			'#required' => TRUE,
			'#value' => $method->call,
	);

	$form['gs']["service2"]["parameters"] = array(
			'#type' => 'textarea',
			'#title' => t('Parameter'),
			'#description' => t("commy separated list. only string or int.  ex: string,int,int,string,string,..."),
			'#required' => FALSE,
			'#value' => $method->parameters,
	);

	$form['gs']["service2"]['description'] = array(
			'#type' => 'textfield',
			'#title' => t('Description'),
			'#description' => t("short description of your service, this can be show on the client side"),
			'#required' => TRUE,
			'#value' => $method->description,
	);
	$form['gs']["service2"]['submit']=array(
			'#type'=>'submit',
			'#value' => t('Save'),
			'#submit' => array('generic_services_admin_method_edit_submit')
	); //Submit button call back.

	return $form;
}


/**
 * Add a generic service form 
 */
function generic_services_admin_add($form,&$form_state){

	$form['gs']["service"] = array(
			'#type' => 'fieldset',
			'#title' => t('Create a generic service'),
			'#description' => t('create a new generic service'),
			'#collapsible' => true,
			'#collapsed' => true,				
			);

	//get the servicetypes
	$options=_generic_services_get_types();
	
	$form['gs']["service"]["servicetype"] = array(
			'#type' => 'select',
			'#title' => t('Typ'),
			'#options' => $options,
			'#description' => t("Bitte den Kontent Typen für Outlook Kontakte auswählen"),
			'#required' => TRUE,
	);


	$form['gs']["service"]['servicename'] = array(
			'#type' => 'textfield',
			'#title' => t('Service Name'),
			'#description' => t("Bitte einen Maschenleserlichen Namen"),
			'#required' => TRUE,
	);

	//dsm($node_field);
	$form['gs']["service"]["command"] = array(
			'#type' => 'textarea',
			'#title' => t('Funktionsauafruf'),
			'#description' => t("Bitte anonyme Parameter als #PARAM1 #PARAM2 #PARAM3"),
			'#required' => TRUE,
	);

	//dsm($node_field);
	$form['gs']["service"]["parameters"] = array(
			'#type' => 'textarea',
			'#title' => t('Parameter'),
			'#description' => t("Comma separated list of input parameters. allowed are: string or int. eg: string,int,int,string"),
			'#required' => FALSE,
	);

	$form['gs']["service"]['description'] = array(
			'#type' => 'textfield',
			'#title' => t('Beschreibung'),
			'#description' => t("Short description of your function"),
			'#required' => TRUE,
	);
	$form['gs']["service"]['submit']=array(
			'#type'=>'submit',
			'#value'=>t('Save'),
			'#submit'=>array('generic_services_admin_add_submit')
	); //Submit button call back.

	return $form;
}

function generic_services_admin_add_submit($form,&$form_state){
	$s_type=$form['gs']['service']['servicetype']['#value'];
	$s_name=$form['gs']['service']['servicename']['#value'];
	$s_command=$form['gs']['service']['command']['#value'];
	$s_params=$form['gs']['service']['parameters']['#value'];
	$s_desc=$form['gs']['service']['description']['#value'];

	db_query("INSERT INTO {gs_methods} (service_name,service_type,method_call,parameters,description)
			VALUES ('".$s_name."',".$s_type.",'".$s_command."','".$s_params."','".$s_desc."');");

}


/**
 * Debug a generic service form
 */
function generic_services_admin_method_debug($form,&$form_state){
	//prettify, if we can
	if(module_exists("prettify"))prettify_add_library();
	
	$options=_generic_services_get_types();
	//dsm($_SESSION['generic_services_debug_result']);


	$method=_generic_services_load_method_call(arg(4));
	$params=explode(',',$method->parameters);
	$form=array();

	///	$form['#redirect'] = 'generic_services_admin_method_debug_result';

	$form['gs'] = array(
			'#type' => 'fieldset',
			'#title' => t('Debugging').': '.$method->name,
			'#description' => t("Type of generic service").': '.$options[$method->type],
	);
	
	$form['gs']["title"] = array(
			'#markup' => '<p><strong>'.t('Service Name').':</strong><br>'.$method->name.
			'<p>'.t('Function Call').':</strong><br><pre><code>'.$method->call.'</code></pre>'.
			'<p><strong>'.t('Parameters').': '.sizeof($params1).'</strong>',
	
			);
	
	$args=array();
	if(isset($_SESSION['generic_services_debug_args']))
		$args=unserialize($_SESSION['generic_services_debug_args']);

	for($i = 0; $i < sizeof($params); $i++){
		$form['gs']['parameter'][$i] = array(
				'#type' => 'textfield',
				'#title' => '#PARAM'.($i+1).' ('.$params[$i].')',
				'#required' => FALSE,
				'#value' => isset($args[$i]) ? $args[$i] :"",

		);
	}

	$form['gs']['parameters']=array(
			'#type' => 'hidden',
			'#value' => $method->parameters,
	); //Submit button call back.

	$form['gs']['name']=array(
			'#type' => 'hidden',
			'#value' => $method->name,
	); //Submit button call back.

	$form['gs']['type']=array(
			'#type' => 'hidden',
			'#value' => $method->type,
	); //Submit button call back.


	$form['gs']['submit']=array(
			'#type'=>'submit',
			'#value' => t('Debug'),
			'#submit' => array('generic_services_admin_method_debug_submit')
	); //Submit button call back.


	if(isset($_SESSION['generic_services_debug_result'])){
		$res=unserialize($_SESSION['generic_services_debug_result']);
		//	dsm('<pre>'.print_r($res['objects'][0]->fields,TRUE).'<pre>');
		$objs=$res['objects'];
		$fields=$res['objects'][0]->fields;
		$header=array();
		$options=array();

		for($i = 0; $i < sizeof($fields); $i++){
			//	dsm($fields[$i]->key." ".$fields[$i]->value);
			$header[]=$fields[$i]->key;
			//$row[]=$fields[$i]->value;
		}

		for($i = 0; $i < sizeof($objs); $i++){
			$row=array();
			for($x = 0; $x < sizeof($objs[$i]->fields); $x++){
				//	dsm($objs[$i]->fields[$x]->key." ".$objs[$i]->fields[$x]->value);
				$row[]=$objs[$i]->fields[$x]->value;
			}
			$rows[]=$row;
		}


		$form['gs']['result']=array(
				'#type'=>'tableselect',
				'#multiple' => false,
				'#header' => $header,
				'#options' => $rows
		); //Submit button call back.

		unset($_SESSION['generic_services_debug_result']);
		$_SESSION['generic_services_debug_result']=null;

	}
	
	return $form;
}


function generic_services_admin_method_debug_submit($form, $form_state){

	$params=explode(',',$form['gs']['parameters']['#value']);
	//dsm($params,sizeof($params));
	for($i = 0; $i < sizeof($params); $i++){
		$args[]=$form_state['input'][$i];

	}
	
	try{
	//lets get the resultset
	$method=_generic_services_load_method_call($form_state['input']['name']);
	$call=_generic_services_replace_params($args,$method->call);
	$result=_generic_services_debug_function_call($method, $call);
		}catch(Exception $ex){
		
		drupal_set_message(__FUNCTION__.':<pre>'.print_r($ex,TRUE).'</pre>','error');
	}
	 
	$_SESSION['generic_services_debug_result']=serialize((array)$result);
	$_SESSION['generic_services_debug_args']=serialize((array)$args);

	return;
}


/**
 * Helper function for debugging function calls
 */
function _generic_services_debug_function_call($method,$call){
	drupal_set_message('<pre>'.$call.'</pre>');
	$form=array();

	if($method->type==0){
		$res=db_query($call);
		if($res->rowCount()==0)return new ArrayOfGenericObjects(new GenericObject(new GenericField("message","Die Abfrage brachte kein Ergebnis")));

		//$result=_generic_services_get_generic_result($res);
		$objs=array();
		foreach($res as $row){
			$result[]=$row;
			$keyvals=array();
			foreach($row as $key =>$value){
				$keyvals[]=new GenericField($key,$value);
			}

			$objs[]=new GenericObject($keyvals);

		}
		$aos=new ArrayOfGenericObjects($objs);
	}

	if($method->type==1){
		$aos= new ArrayOfGenericObjects(new GenericObject(new GenericField('message',t('Die Anfrage brachte kein Ergebnis'))));		
		try{
			$node=eval($call);
			
			//if result is array
			if(sizeof($node)>1)foreach($node as $obj){
				$genobj=_generic_services_get_generic_result((array)$obj);
				$result[]=$genobj;

			}
			if($node)$result[]=_generic_services_get_generic_result((array)$node);

			$aos= new ArrayOfGenericObjects($result);
		}catch(Exception $ex){
			watchdog('generic services', 'Drupal Exec:'.$call.'<pre>%vars</pre>',array("%vars"=>print_r($ex,TRUE)),WATCHDOG_ERROR);
		}
	}

	if($method->type==2){
		$result=eval($call);
		$ret[]=new GenericField("message","Die Abfrage lieferte kein Ergebnis");
		if(!empty($result))$ret[0]->value=$result;
		$aos= new ArrayOfGenericObjects(new GenericObject($ret));
	}
	
	if(module_exists('devel')){
		dsm($method,'Service');
		dsm($result,'Original Result');
		dsm($aos,'Generic Result');
	}else{
		drupal_set_message('Generic Service:<pre>'.print_r($method,TRUE).'</pre>');
		drupal_set_message('Original Result:<pre>'.print_r($result,TRUE).'</pre>');
		drupal_set_message('Generic Result:<pre>'.print_r($aos,TRUE).'</pre>');		
	}
	
	return $aos;
}


?>