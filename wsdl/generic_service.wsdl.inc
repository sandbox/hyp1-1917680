<?php
/**
 * @file
 * This file is a template for WSDL construction by soap_server.
 */

$wsdl_content ="<?xml version='1.0' encoding='UTF-8'?>
<wsdl:definitions name='gs' targetNamespace='http://kimo2002.no-ip.org/gs/' xmlns:wsdl='http://schemas.xmlsoap.org/wsdl/' xmlns:tns='http://kimo2002.no-ip.org/gs/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/wsdl/soap/'>
<wsdl:types><xsd:schema xmlns:xsd='http://www.w3.org/2001/XMLSchema' targetNamespace='http://kimo2002.no-ip.org/gs/'>
	<xsd:complexType name='ArrayOfGenericObjects'>
		<xsd:sequence>
			<xsd:element name='objects' type='tns:GenericObject' maxOccurs='unbounded' minOccurs='0'></xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name='GenericObject'>
		<xsd:sequence>
			<xsd:element name='fields' type='tns:GenericField' maxOccurs='unbounded' minOccurs='0'></xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name='GenericField'>
		<xsd:sequence>
			<xsd:element name='key' type='xsd:string'></xsd:element>
			<xsd:element name='value' type='xsd:string'></xsd:element>
		</xsd:sequence>
	</xsd:complexType>
</xsd:schema></wsdl:types>
$requests

$responses
  <wsdl:portType name='soap_server_port_type'>
$port_type_operations
  </wsdl:portType>
    <wsdl:binding name='GenericServiceBinding' type='tns:soap_server_port_type'>
<soap:binding style='rpc' transport='http://schemas.xmlsoap.org/soap/http' />
    
$binding_operations
  </wsdl:binding>
    
  <wsdl:service name='GenericServiceSoap'>
    <wsdl:port name='soap_server_port' binding='tns:GenericServiceBinding'>
      <soap:address location='$service_endpoint'/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>";
