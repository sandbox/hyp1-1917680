<?php
/*
 * API für Generic Serivces
 * 
 */
class GenericField{
	public $key;
	public $value;
	function __construct($key = "empty",$value = "empty"){
		$this->key = (string)$key;
		$this->value = (string)$value;
	}
}

class GenericObject{
	public $fields=array();
	function __construct($fields=array()){
//if one object we create array!
		if(is_array($fields))$this->fields=$fields;
		else $this->fields[]=$fields;
	}

}

class ArrayOfGenericObjects{
	public $objects = array();
	function __construct($objects = array()){
//if one object we create array!
		if(is_array($objects))$this->objects = $objects;
		else $this->objects[] = $objects;
	}	
}

?>